﻿namespace CRMD.CPSA.NotePlugin
{
    using CRMD.CPSA.NotePlugin.Helper;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using System;
    using System.Data;
    using System.IO;

    public class NoteCreated : PluginBase, IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            string step = string.Empty;
            try
            {
                base.Initialize(serviceProvider);
                
                if (this.Context.MessageName.ToLower() != "create" || this.Context.Depth > 1)
                    return;

                var entity = this.TargetEntity;

                step = "Retrieve note regarding object.";
                this.Tracing.Trace(step);
                EntityReference regarding = entity.GetAttributeValue<EntityReference>("objectid");

                step = string.Format("Note regarding object is '{0}'.", regarding.LogicalName);
                this.Tracing.Trace(step);
                if (regarding.LogicalName == "crmd_corporateupload")
                {
                    Entity corporateUpload = this.Service.Retrieve(regarding.LogicalName, regarding.Id, new ColumnSet("crmd_account"));

                    int total = 0,
                        created = 0,
                        error = 0;
                    int status = 0,
                        inProgressStatus = 103460000,
                        failedStatus = 103460001,
                        completedStatus = 103460002;

                    // update corporate upload status
                    corporateUpload["crmd_processstatus"] = new OptionSetValue(inProgressStatus);
                    corporateUpload["crmd_processstarted"] = DateTime.UtcNow;
                    this.Service.Update(corporateUpload);

                    step = string.Format("Update corporate process details. Total={0}, Successful={1}, Error={2}.", total, created, error);
                    this.Tracing.Trace(step);

                    try
                    {
                        step = "Note created for corporate upload";
                        this.Tracing.Trace(step);

                        step = "Read data from attachment";
                        this.Tracing.Trace(step);

                        DataTable data = this.ReadDataFromAttachment(entity);

                        step = string.Format("Total records retrieve from attachment: {0}", data.Rows.Count);
                        this.Tracing.Trace(step);

                        total = data.Rows.Count;
                        foreach (DataRow row in data.Rows)
                        {
                            try
                            {
                                Entity contact = this.BuildContact(row);
                                contact["parentcustomerid"] = corporateUpload["crmd_account"];
                                this.Service.Create(contact);
                                created++;
                            }
                            catch (Exception ex)
                            {
                                this.Tracing.Trace(string.Format("Error occured in creating contact. Error: {0}.", ex.Message));
                                error++;
                            }
                        }

                        status = completedStatus;
                    }
                    catch(Exception ex)
                    {
                        this.Tracing.Trace(string.Format("Error:{0}. The error occured at {1}.", ex.Message, step));
                        status = failedStatus;
                    }
                    finally
                    {
                        // update corporate record process details
                        step = string.Format("Update corporate process details. Total={0}, Successful={1}, Error={2}.", total, created, error);
                        this.Tracing.Trace(step);

                        corporateUpload["crmd_totalrecords"] = total;
                        corporateUpload["crmd_successfulrecords"] = created;
                        corporateUpload["crmd_errorrecords"] = error;
                        corporateUpload["crmd_processstatus"] = new OptionSetValue(status);
                        corporateUpload["crmd_processcompleted"] = DateTime.UtcNow;

                        this.Service.Update(corporateUpload);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Tracing.Trace(string.Format("Error:{0}. The error occured at {1}.", ex.Message, step));
                throw new InvalidPluginExecutionException("An error occured in Corporate upload note creation, please check tracing logs for more info.");
            }
        }

        private DataTable ReadDataFromAttachment(Entity entity)
        {
            DataTable dataTable = new DataTable();
            DataRow row;

            string body = entity.GetAttributeValue<string>("documentbody");
            if (!string.IsNullOrWhiteSpace(body))
            {
                byte[] content = Convert.FromBase64String(body);
                using (var stream = new StreamReader(new MemoryStream(content)))
                {
                    string header = stream.ReadLine();
                    foreach (string columnHeader in CSVHelper.ParseCSVRow(header))
                    {
                        dataTable.Columns.Add(new DataColumn(columnHeader));
                    }

                    string line;
                    while ((line = stream.ReadLine()) != null)
                    {
                        row = dataTable.NewRow();
                        int index = 0;
                        foreach (string data in CSVHelper.ParseCSVRow(line))
                        {
                            row[index++] = data;
                        }
                        dataTable.Rows.Add(row);
                    }
                }
            }

            return dataTable;
        }

        private Entity BuildContact(DataRow row)
        {
            Entity contact = new Entity("contact");

            contact["firstname"] = row[0];
            contact["lastname"] = row[1];
            contact["emailaddress1"] = row[2];
            contact["telephone1"] = row[3];
            contact["address1_line1"] = row[4];
            contact["address1_line2"] = row[5];
            contact["address1_city"] = row[6];
            contact["address1_stateorprovince"] = row[7];
            contact["address1_postalcode"] = row[8];
            contact["address1_country"] = row[9];
            contact["crmd_contactsource"] = new OptionSetValue(103460002);

            Entity provinceState = StateProvinceLookupHelper.RetrieveByName(this.Service, (string)row[7]);
            if (provinceState != null)
                contact["crmd_province"] = new EntityReference(provinceState.LogicalName, provinceState.Id);

            Entity country = CountryLookupHelper.RetrieveByName(this.Service, (string)row[9]);
            if (country != null)
                contact["crmd_country"] = new EntityReference(country.LogicalName, country.Id);

            return contact;
        }
    }
}