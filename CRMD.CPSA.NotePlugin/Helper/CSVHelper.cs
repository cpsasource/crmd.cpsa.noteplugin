﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRMD.CPSA.NotePlugin.Helper
{
    public class CSVHelper
    {
        public static List<string> ParseCSVRow(string row)
        {
            List<string> result = new List<string>();
            string[] data = row.Split(',');

            for (int i = 0; i < data.Length; i++)
            {
                string value = BuildCSVData(data, ref i);
                if (StartsWithCSVQuote(value))
                    value = value.Substring(1, value.Length - 2);

                result.Add(value.Replace("\"\"", "\""));
            }

            return result;
        }

        static string BuildCSVData(string[] data, ref int index)
        {
            StringBuilder value = new StringBuilder();
            value.Append(data[index]);
            if (StartsWithCSVQuote(value.ToString()) && !EndsWithCSVQuote(value.ToString()))
            {
                index++;
                value.Append(",");
                value.Append(BuildCSVData(data, ref index));
            }
            else if (EndsWithCSVQuote(value.ToString()))
            {
                return value.ToString();
            }

            return value.ToString();
        }

        static bool StartsWithCSVQuote(string data)
        {
            if (data.StartsWith("\""))
            {
                int occurrence = 0;
                foreach (char c in data)
                {
                    if (c != '\"')
                        break;
                    occurrence++;
                }

                return occurrence % 2 == 1;
            }

            return false;
        }

        static bool EndsWithCSVQuote(string data)
        {
            if (data.EndsWith("\""))
            {
                int occurrence = 0;
                foreach (char c in data.Reverse())
                {
                    if (c != '\"')
                        break;
                    occurrence++;
                }

                return occurrence % 2 == 1;
            }

            return false;
        }
    }
}
